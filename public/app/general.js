angular.module('Qsystem')
 
.factory('generalFactory',
    ['$http', '$compile',
    function ( $http, $compile ) {
    	var service = {};
        
        service.getHandler = function (url, callback){
        	$http.get(url)
                .success(function (response) {
                	//console.log(response);
                	callback(response);
                }).error(function(response) {
                	callback(response);
                	console.log(response);
			    });
        };

		service.getHandlerExtra = function (url, params, callback){
        	$http.get(url, {params: {alias: params}})
                .success(function (response) {
                	//console.log(response);
                	callback(response);
					console.log("-------response");
					console.log(response);
                }).error(function(response) {
                	callback(response);
                	console.log(response);
			    });
        };
        
        service.postHandler = function (url, params, callback){
        	$http.post(url, params)
                .success(function (response) {
                	callback(response);
                }).error(function(response) {
                	callback(response);
                	console.log(response);
			    });
        };
        
       service.loginAuth = function(callback){
        	$http.get('/login/check')
	            .success(function (response) {
	            	callback(response);
	            }).error(function(response) {
	            	callback(response);
	            	console.log(response);
			    });
        };
        
    	return service;
    }]
);