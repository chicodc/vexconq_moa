var app = angular.module('Quetv', ['ngAnimate','ui.bootstrap', 'ngIdle', 'ds.clock']);

app.filter('html', ['$sce', function ($sce) {
    return function (text) {
        return $sce.trustAsHtml(text);
    };
}])

app.controller('QuetvController',[ '$timeout', '$scope','$rootScope','$route', '$compile', '$uibModal','$log', '$routeParams', '$location', 'generalFactory', 'socket',
	function( $timeout, $scope, $rootScope, $route, $compile, $uibModal, $log, $routeParams, $location, generalFactory, socket){
	$scope.format = 'dd-MMM-yyyy hh:mm:ss a';

	$scope.session_check = function(admin){
		if(admin){
			generalFactory.getHandler('/login/check', function(response){
				if(response.loggedin === true){
					$location.path('/quetvAdmin');
				}else{
					console.log(response.loggedin);
					$location.path('/');
				}
			});
		}else{
			generalFactory.getHandler('/login/ioCheck', function(response){
				if(response.iologgedin == true){
					$location.path('/quetv');
				}else{
					console.log(response.iologgedin);
					$location.path('/iologin/tv');
				}
			});
		}
	};


	socket.on('newPendingAdded', function(){
		socket.getSocket().removeAllListeners();
		$route.reload();
	});
	socket.on('reloadQuetv', function(){
		socket.getSocket().removeAllListeners();
		console.log('called');
		$route.reload();
	});
	socket.on('calledClient', function(data){
		socket.getSocket().removeAllListeners();
		console.log(data.data);
		call();
	});

	$scope.initquetv = function(admin){
		$scope.session_check(admin);

		generalFactory.getHandler('/quetv/initialize', function(response){
			$scope.counters = response.counter;
			$scope.called_client = response.called_client;
			$scope.unhide = 'hide';
		});

		generalFactory.getHandler('/quetv/getpending/notall', function(response){
			$scope.pending = response.pending;
			$scope.pending_count = response.pending_count.count;
			var slider_count_per_page = 4;

			for(var i=1; i<=5; i++){
				chunk_result = chunk(i,$scope.pending, slider_count_per_page);
				$scope['chunkedData' +i] = chunk_result[0];
				$scope['toslide' + i] 	 = chunk_result[1];
			};
			$timeout(function () {$scope.$broadcast('dataloaded');}, 0, false);
			}
		);
	};

    function call(){
    	generalFactory.getHandler('/quetv/initialize', function(response){
			if(response.called_client[0].banner_name != null){
				$scope.called_client = response.called_client;
				$scope.counters = response.counter;
				$scope.unhide = 'unhide';
				//console.log($scope.called_client);
				$scope.call = true;
				$scope.$broadcast('called', {counter: response.called_client[0].banner_num, route : $route});
			}
		});
    }


	function chunk(status,arr, size) {
		var newArr0 = []
	  var newArr1 = [];
		var x = 1 ;
		for (var i=0; i<arr.length; i++){
			if(arr[i].status == status){
				arr[i]['pos'] = x ;
				x = x + 1;
				newArr0.push(arr[i]);
			}
		}

	  for (var i=0; i<newArr0.length; i+=size) {
	    newArr1.push(newArr0.slice(i, i+size));
	  }

		console.log(newArr1);
		if(newArr0.length > size){
			return [newArr1,"slide"];
		}
	  return [newArr0,"position_flip "];
	}


}]);

app.directive('slideelem', ['$timeout', function ($timeout) {
  return {
    link: function ($scope, element, attrs) {
      $scope.$on('dataloaded', function () {
        $timeout(function () {
        // $('.slide').children().not(':first').hide();
		    $('.slide').list_ticker({
					speed:7000,
					effect:'slide'
				});
			$(".rotate").textrotator({
			    animation: "flipUp",
			    speed: 4000
			});
        }, 0, false);
      });
      $scope.$on('called', function (event, args) {
      	$('#modal-container').modal('hide');
        $timeout(function () {
    	var advideo = document.getElementById("ad-video");
        localStorage.setItem("advideo", advideo.currentTime);
        console.log(args);
		$('#sound_element').html('<audio autoplay><source src="../sound/sound1.mp3" type="audio/mp3"></audio>');
		$('#nosit'+args.counter).blink({
							maxBlinks: 5,
							speed: 100,
							onMaxBlinks:
							function() {
								$('.bannersf').delay(1000).fadeOut('750');
								args.route.reload();
							}
					});
        }, 0, false);
      });
    }
  };
}]);
