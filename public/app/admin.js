function generatePassword() {
    var length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}

var app = angular.module('Admin', ['ngAnimate','ui.bootstrap']);

app.controller('AdminController',[ '$scope','$rootScope','$route', '$q', '$timeout','$anchorScroll', '$uibModal','$log', '$routeParams', '$location', 'generalFactory', 'socket',
	function( $scope, $rootScope, $route, $q, $timeout, $anchorScroll, $uibModal, $log, $routeParams, $location, generalFactory, socket){

	$scope.promptType;
	$scope.promptMessage = '';
	$scope.prompt = false;
	$scope.calledFromScope;

	$scope.usernameTemp = '';
	$scope.passwordTemp = '';
	$scope.modalTitle = '';

	$scope.currentTab = 3;
	$scope.collapsed = {};
	$scope.collapseOpen = {};
	$scope.scrollLocation = '';
	$scope.userids;
	generalFactory.getHandler('/admin/settings', function(response){
		if(response.success){
				$scope.userids = response.success;
				$scope.userids.forEach(function(user){
					$scope.collapsed[user.id] = false;
					$scope.collapseOpen[user.id] = false;
				});
		}
	});

	//PPERMISSIONS
	$scope.ADD_USER = $scope.$parent.ADD_USER;
	$scope.EDIT_USER = $scope.$parent.EDIT_USER;
	$scope.DELETE_USER = $scope.$parent.DELETE_USER;
	$scope.TOGGLE_ACTIVATION = $scope.$parent.TOGGLE_ACTIVATION;
	$scope.RESET_PASSWORD = $scope.$parent.RESET_PASSWORD;
	$scope.TOGGLE_PERMISSIONS = $scope.$parent.TOGGLE_PERMISSIONS;
	$scope.TOGGLE_MENU = $scope.$parent.TOGGLE_MENU;
	$scope.CHANGE_LEVEL = $scope.$parent.CHANGE_LEVEL;

	$scope.logout = function(){
		generalFactory.getHandler('/logout', function(response){
			if(response.destroyed){
				 $location.path('/');
			}
		});
	};

	$scope.back = function(){
		$location.path('/menu');
	};

	$scope.setActiveTab = function(id){
		$scope.currentTab = id;
	};

	$scope.setCurrentScroll = function(button,id){
		$scope.scrollLocation = button+''+id;
	};

	$scope.setActiveCollapse = function(id){
		$scope.collapsed[id] = !$scope.collapsed[id];

	};

	$scope.callprompt = function(){
		if($scope.calledFromScope){
			$scope.prompt = true;

			$timeout(function () {
				$scope.$broadcast('prompt');
			});
		}
		if($scope.type === 'ERROR'){
			$scope.alert = 'danger';
			$scope.strong = 'Oh snap!';
			console.log(message);
		} else {
			$scope.alert = 'success';
			$scope.strong = 'Well done!';
		}
	};

	$scope.callindividualprompt = function(id){

		$scope.individualPrompt[id] = true;
		$timeout(function () {
			$scope.$broadcast('indiprompt', {id : id});
		});
		if($scope.type === 'ERROR'){
			$scope.alert = 'danger';
			$scope.strong = 'Oh snap!';
			console.log(message);
		} else {
			$scope.alert = 'success';
			$scope.strong = 'Well done!';
		}
	};

	$scope.initializeAdmin = function(){

		$scope.userlevel = $scope.$parent.userlevel;
		$scope.account_level = {};

		generalFactory.getHandler('/admin/levels', function(response){
			$scope.user_levels = response.success;
		});

		$scope.calledFromScope = false;
		$scope.prepareTable();

		if($scope.scrollLocation != ''){
			$(window).scrollTop($('#'+$scope.scrollLocation).offset().top);
		}

	};

	$scope.prepareTable = function(){

		var permission = {};
		var menu = {};
		var prompts = {};

		$scope.users;

		var adminPromise = $q.all([
			generalFactory.getHandler('/admin/settings', function(response){
			if(response.success){
				$scope.users = response.success;
				$scope.users.forEach(function(user){

					prompts[user.id] = false;

					permission["user"+user.id] = {};
					user.permissions.forEach(function(item){
						permission["user"+user.id]["item"+item.id] = [item.permission_id, user.id];
					});

					menu["user"+user.id] = {};
					user.menus.forEach(function(item){
						menu["user"+user.id]["item"+item.id] = [item.menu_id, user.id];
					});
					$scope.account_level[user.id] = user.level;
				});
				$scope.permission = permission;
				$scope.menu = menu;
				$scope.individualPrompt = prompts;
			}else{
				console.log(response.err);
			}
		})
		]);

		adminPromise.then(function(){
			$timeout(function(){
				$('#loading_screen_child').fadeOut();
				$(".userList").fadeIn();
				$scope.callprompt();
			}, 3000, false);
		});
	};

	$scope.toggleActivate = function(status, id, username){

		$timeout(function () {
			$(".userList").hide();
			$('#loading_screen_child').show();
		}, 0, false);

		var newStatus = '';
		if(status == 'No'){
			newStatus = 'Yes';
			active = 'ACTIVATED';
		}else{
			newStatus = 'No';
			active = 'DEACTIVATED';
		}
		generalFactory.postHandler('/admin/activate', {status : newStatus, id : id},function(response){
			if(response.success == 'ERROR'){
				$scope.message = 'Error on data save';
				console.log($scope.message);
			} else {
				$scope.message = response.success;
				$scope.promptType = 'SUCCESS';
				$scope.promptMessage = "User : "+ username +" is now "+ active +".";
				$scope.calledFromScope = true;
				$scope.prepareTable();
			}
		});
	};


	$scope.resetPass = function(newusername, id){
		$timeout(function () {
			$(".userList").hide();
			$('#loading_screen_child').show();
		}, 0, false);

		var newpassword = generatePassword();
		var resetPromise = $q.all([ generalFactory.postHandler('/admin/reset', {username : newusername, password : newpassword}, function(response){
			if(response.success != 'ERROR'){
				$scope.modalTitle = 'PASSWORD RESET SUCCESSFULL';
				$scope.usernameTemp = newusername;
				$scope.passwordTemp = newpassword;

				$scope.promptType = 'SUCCESS';
				$scope.promptMessage = "Password reset for user "+newusername+" was successfull.";
				$scope.calledFromScope = true;

				$scope.prepareTable();
			}
		})
		]);

		resetPromise.then(function(){
			$timeout(function(){
				$scope.openmodal('', 'partials/form_newUser.html');
			}, 3000, false);
		});
	};

	$scope.deleteuser = function(id){
		var delete_confirmed = confirm("Are you sure you want to delete user?");
		if(delete_confirmed === true){

			$timeout(function () {
				$(".userList").hide();
				$('#loading_screen_child').show();
			}, 0, false);

			generalFactory.getHandler('/admin/delete/'+id, function(response){
				if(response.success == 'ERROR'){
					$scope.message = 'Error on data save';
					console.log($scope.message);
				} else {
					$scope.message = response.success;

					$scope.promptType = 'SUCCESS';
					$scope.promptMessage = "USER DELETED!";
					$scope.calledFromScope = true;

					$scope.prepareTable();
				}
			});
		}
	};

	$scope.toggleSetting = function(data, user, setting){
		if(setting === 'permission'){
			formdata = {permissions : data, user_id : user};
		}else{
			formdata = {menus : data, user_id : user};
		}
		$scope.selected = data;
		generalFactory.postHandler('/admin/'+setting, formdata, function(response){
			$scope.promptType = response.success;
			$scope.promptMessage = response.message;
			$scope.callindividualprompt(user);
		});
	};


	$scope.changeLevel = function(level, user){
		generalFactory.postHandler('/admin/level', {level: level, user_id : user}, function(response){
			$scope.promptType = response.success;
			$scope.promptMessage = response.message;
			$scope.callindividualprompt(user);
		});
	};

	$scope.checkSubs = function(user, parent, data , subkeys){
		var subscount = Object.keys(subkeys).length;
		if(subscount){
			var me = $scope.menu['user'+user]['item'+parent];
			if(me[0] != null){
				Object.keys(subkeys['sub'+user+''+parent]).map(function(k) {
					var sub = subkeys['sub'+user+''+parent][k];
					$scope.menu['user'+user]['item'+sub] = [sub, user];
				});
			}else{
				Object.keys(subkeys['sub'+user+''+parent]).map(function(k) {
					var sub = subkeys['sub'+user+''+parent][k];
					$scope.menu['user'+user]['item'+sub] = [null, user];
				});
			}
		}
	};

	$scope.checkParent = function(user, parent, data){
		var count = 0;
		Object.keys(data).map(function(k) {if(data[k][0] != null){count++;} });

		if(count === 0){
			$scope.menu['user'+user]['item'+parent] = [null, user];
		}else{
			$scope.menu['user'+user]['item'+parent] = [parent, user];
		}
	};


    $scope.forceLogout = function(user, id){
		generalFactory.postHandler('/admin/logoutUser', {username : user, id : id}, function(response){
			if(response.success == 'ERROR'){
				$scope.message = 'Error on data save';
				console.log($scope.message);
			} else {
                if(response.success == 'ACTIVE'){
					alert('This user is currently assisting a client!');
				}else{
					$scope.message = response.success;
					$scope.promptType = 'SUCCESS';
					$scope.promptMessage = "User : "+ user +" is now logged out.";
					$scope.calledFromScope = true;
					$scope.prepareTable();
				}
			}
		});
	};

	$scope.openmodal = function (size, url) {
    	var modalInstance = $uibModal.open({
	      	animation: true,
	      	templateUrl: url,
	      	controller: 'AddUserController',
	      	size: size,
	      	scope: $scope,
	      	windowClass: 'app-modal-window',
	      	resolve: {
	        		items: function () {
	          		return $scope;
	        	}
	     	}
	    });

	    modalInstance.result.then(function (selectedItem) {
	      	$scope.selected = selectedItem;
	    }, function () {
	      	$log.info('Modal dismissed at: ' + new Date());
	    });
	};

}]);


app.controller('AddUserController' , ['items', '$scope', '$route', '$timeout','$uibModalInstance', '$location', 'generalFactory',
	function (items, $scope, $route, $timeout, $uibModalInstance, $location, generalFactory ) {
	$scope.levels = items.user_levels;

	$scope.cancel = function () {

		$scope.$parent.usernameTemp = '';
		$scope.$parent.passwordTemp = '';
	   	$uibModalInstance.dismiss('cancel');
	};


	$scope.addUser = function(){
		var password = generatePassword();
		$scope.password = password;

		var formData = {
			firstname : $scope.firstf,
			lastname : $scope.lastf,
			username : $scope.username,
			password : $scope.password,
			level : $scope.account_type,
			change : 0
		};

		$scope.$parent.modalTitle = 'ADDED USER SUCCESSFULLY';
		$scope.$parent.usernameTemp = $scope.username;
		$scope.$parent.passwordTemp = password;


		generalFactory.postHandler('/admin/add', formData, function(response){

			if(response.success != 'ERROR'){
				$scope.firstf = '';
				$scope.lastf = '';
				$scope.account_type = '';
				$scope.username = '';
				$scope.password = '';
				$timeout(function () {
					$(".userList").fadeIn();
				}, 1000, false);

				$scope.$parent.prepareTable();
				$scope.$parent.openmodal('', 'partials/form_newUser.html');
				$scope.promptType = response.success;
				$scope.promptMessage = response.message;
				$scope.calledFromScope = true;

				$scope.callprompt();
				$uibModalInstance.dismiss('cancel');
			}else{
				console.log(response);
			}
		});
	};
}]);

app.directive('messagealert', ['$timeout', function ($timeout) {
  return {
    link: function ($scope, element, attrs) {
    	$scope.$on('prompt', function () {
    		$timeout(function () {
    			$("#alert").fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        	}, 0, false);
    	});
    }
  };
}]);

app.directive('individualmessagealert', ['$timeout', function ($timeout) {
  return {
  	multiElement: true,
    link: function ($scope, element, attrs) {
    	$scope.$on('indiprompt', function (event, args) {
    		$timeout(function () {
    			$("#individual"+args.id).fadeIn( 300 ).delay( 1500 ).fadeOut( 400 );
        	}, 0, false);
    	});
    }
  };
}]);
