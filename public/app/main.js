var app = angular.module('Main', ['ngAnimate','ui.bootstrap', 'ngCookies', 'ngIdle']);

app.controller('MainController',['$scope','$rootScope','$route', '$q', '$interval', '$timeout', '$window', '$uibModal','$log', '$routeParams', '$cookies','$location', 'generalFactory', 'socket', 'Idle',
	function( $scope, $rootScope, $route, $q, $interval, $timeout, $window, $uibModal, $log, $routeParams, $cookies, $location, generalFactory, socket, Idle){

	$scope.clock;
	var mm = 0, mms = 0;
	$scope.seconds = 0;
	$scope.minutes = 0;
	$scope.hours = 0;
	$scope.padSeconds = "00";
	$scope.padMinutes = "00";
	$scope.padHours = "00";

	$scope.changedCounter = false;


	/*FOR IDLE TIME*/
	$scope.$on('IdleStart', function() {
        console.log('Hey your doing nothing!');
    });

    $scope.$on('IdleWarn', function(e, countdown) {

        // follows after the IdleStart event, but includes a countdown until         the user is considered timed out
        // the countdown arg is the number of seconds remaining until then.
        // you can change the title or display a warning dialog from here.
        // you can let them resume their session by calling Idle.watch()
    });

    $scope.$on('IdleTimeout', function() {
    	console.log("You're dead!");
    	$scope.logout();
        // the user has timed out (meaning idleDuration + timeout has passed     without any activity)
        // this is where you'd log them
    });

    $scope.$on('IdleEnd', function() {
    	console.log('Im bACK');
        // the user has come back from AFK and is doing stuff. if you are     warning them, you can use this to hide the dialog
    });

    $scope.$on('Keepalive', function() {
        // do something to keep the user's session alive
    });
    /*END FOR IDLE TIME*/


	$scope.initMain = function(){
		Idle.watch();
		$scope.counter = '';
		$scope.transaction = '';
		$scope.available = '';
		$scope.frontcounters;

		generalFactory.loginAuth(function(response){
			$scope.auth = response;
			console.log($scope.auth);

			if($scope.auth.loggedin === true){
				$scope.name = $scope.auth.name;
				$scope.userlevel = $scope.auth.level;
				$scope.user_id = $scope.auth.userid;

				$scope.setCounter();
				$scope.setMenu();
				$scope.setPermissions();
				$scope.setCounterOptions();

			}else{
				window.location = '/';
			}
		});
	};

	$scope.setCounter = function(){
		generalFactory.getHandler('/frontline/handle', function(response){
			$scope.counter = response.success.counter_number;
			$scope.transaction = response.success.service;
			$scope.status = response.success.servicing;
			$scope.available = response.availability;

			if($scope.available === 1){
				$(".pageheader").toggle("blind");
				$(".breaky").html($('.breaky').html() == '<a href="#"><i class="fa fa-fw fa-bell"></i> LETS GO!</a>' ? '<a href="#"><i class="fa fa-fw fa-bell-slash"></i> BREAK TIME</a>' : '<a href="#"><i class="fa fa-fw fa-bell"></i> LETS GO!</a>');
			}
			$scope.changedCounter = false;
		});
	};

	$scope.setMenu  = function(){
		generalFactory.getHandler('/frontline/getmenu', function(response){
			$scope.menu = response;
		});
	};

	$scope.setPermissions = function(){
		generalFactory.getHandler('/frontline/getpermission', function(response){
			$scope.userpermission = response.success;

			$scope.userpermission.forEach(function(element){
				if(element.user_id != null){
					$scope[element.permission] = true;
				}else{
					$scope[element.permission] = false;
				}
			});
		});
	};

	$scope.setCounterOptions = function(){
		generalFactory.getHandler('/frontline/getcounter', function(response){
			$scope.frontcounters = response.success;
		});
	};

	socket.on('reloadQuetv', function(){
		$scope.initMain();
	});

	// socket.on('connect_error', function(){
	// 	alert('Oops! Connection to the sever was lost, please contact the systems admin.');
	// 	window.location.reload();
	// });

	$scope.toggleHead = function(){
		generalFactory.getHandler('/frontline/breaktime', function(response){
			if(response.success == 'ERROR'){
				$scope.message = 'Error on data save';
			} else {
				$scope.available ^= 1;
				$(".pageheader").toggle("blind");
				$(".breaky").html($('.breaky').html() == '<a href="#"><i class="fa fa-fw fa-bell"></i> LETS GO!</a>' ? '<a href="#"><i class="fa fa-fw fa-bell-slash"></i> BREAK TIME</a>' : '<a href="#"><i class="fa fa-fw fa-bell"></i> LETS GO!</a>');
				socket.emit('refreshQuetv');
				$scope.message = response.success;
			}
		});
	};

	$scope.vacateCounter = function(){
		var promise = $q.all([
			generalFactory.getHandler('/frontline/vacateCounter', function(response){
				return response;
			})
		]);

		promise.then(function(value){
			if(value.success === 'ACTIVE'){
		  		alert(response.message);
	  		}else{
	  			$scope.initMain();
	  			$scope.changedCounter = false;
  				socket.emit('refreshQuetv');
	  		}
		});
	};

	$scope.openM = function (size, url) {
    	var modalInstance = $uibModal.open({
	      	animation: true,
	      	templateUrl: url,
	      	controller: 'ModalInstanceCtrl',
	      	size: size,
	      	scope: $scope,
	      	windowClass: 'app-modal-window',
	      	resolve: {
	        		items: function () {
	          		return $scope;
	        	}
	     	}
	    });

	    modalInstance.result.then(function (selectedItem) {
	      	$scope.selected = selectedItem;
	    }, function () {
	      	$log.info('Modal dismissed at: ' + new Date());
	    });
	};

	$scope.clickpage = function(page, sub){
		$scope.page = page;
		$('.sub').removeClass('sub_active');
	};

	$scope.logout = function(){
		generalFactory.getHandler('/logout', function(response){
			if(response.destroyed){
				$rootScope = $rootScope.$new(true);
				$scope = $scope.$new(true);
				socket.emit('refreshQuetv');
				$location.path('/');
			}
		});
	};

	$scope.refreshFront = function(){
		$scope.$broadcast('refreshFront');
	};

	$scope.showTV = function(){
		$location.path('/quetvAdmin');
	};

	$scope.showList = function(){
		$location.path('/quelist');
	};

	$scope.showIPAD = function(){
		if($window.navigator.userAgent.match(/iPad/i)){
			$location.path('/ipadque');
		}else{
		   	alert('Please open this page on an IPAD. Thank you!');
		}
	};

	$scope.counterChanged = function(){
		socket.emit('refreshQuetv');
		$route.reload();
		/*var promises = $q.all([
			$scope.setCounter(),
			$scope.setCounterOptions()
		]);
		promises.then(function(){$scope.$broadcast ('closeModal');});*/
	};

	$scope.timer = function(){
   		$interval.cancel($scope.clock);
   		$timeout.cancel();

       	$scope.seconds++;
        if($scope.seconds === 60){
        	$scope.seconds = 0;
        	$scope.minutes++;
        }
        if($scope.minutes === 60){
        	$scope.minutes = 0;
        	$scope.hours++;
        }

        if($scope.seconds < 10){ $scope.padSeconds = "0" + $scope.seconds.toString(); }else{$scope.padSeconds = $scope.seconds;}
        if($scope.minutes < 10){ $scope.padMinutes = "0" + $scope.minutes.toString(); }else{$scope.padMinutes = $scope.minutes;}
        if($scope.hours < 10){ $scope.padHours = "0" + $scope.hours.toString(); }else{$scope.padHours = $scope.hours;}


		$cookies.put('lastSeconds', $scope.seconds);
		$cookies.put('lastMinutes', $scope.minutes);
		$cookies.put('lastHours', $scope.hours);

        $scope.clock = $interval($scope.timer,1000);

   	};

   	$scope.stopTime = function(){
   		console.log('stopped');
   		$interval.cancel($scope.clock);
   	};

   	$scope.startTime = function(){
   		$scope.clock = $interval($scope.timer,1000);
   	};

   	$scope.resetTime = function(){
   		$scope.seconds = 0;
   		$scope.minutes = 0;
   		$scope.hours = 0;

   		$cookies.remove('lastSeconds');
		$cookies.remove('lastMinutes');
		$cookies.remove('lastHours');
		$cookies.remove('clock');

   		$cookies.put('lastSeconds', $scope.seconds);
		$cookies.put('lastMinutes', $scope.minutes);
		$cookies.put('lastHours', $scope.hours);
		$cookies.put('clock', false);
   	};

   	$scope.jumped = function(){
   		$scope.$broadcast('jumped');
   	};
}]);

app.controller('ModalInstanceCtrl' , ['items', '$scope', '$rootScope', '$route','$q', '$uibModalInstance', '$location', 'generalFactory',
	function (items, $scope, $rootScope, $route,$q, $uibModalInstance, $location, generalFactory ) {

	$scope.counters = items.frontcounters;
	$scope.currentcounter = items.counter;
	$scope.newCounter = ""+items.counter+"";

	$scope.getStatus = function(){
		generalFactory.getHandler('/main/getServices', function(response){
			if(response.status === "SUCCESS"){
				$scope.services = response.result;
			}
		});
	};

	for ( var index=0; index<$scope.counters.length; index++ ) {
	    if ( $scope.counters[index].counter_number == $scope.currentcounter ) {
	    	$scope.counters[index].mysit = 'Me';
	    	$scope.counters[index].disabled = 'enabled';
	    }else{
	    	$scope.counters[index].mysit = $scope.counters[index].sit;
	    	if($scope.counters[index].sit != null){
	    		$scope.counters[index].disabled = 'disabled';
	    	}else{
	    		$scope.counters[index].disabled = 'enabled';
	    	}
	    }
	}

	$scope.changeCounter = function(){
	  	$scope.$parent.changedCounter = true;
	  	if($scope.newCounter == undefined){$scope.newCounter = '';}
		if($scope.newTransaction == undefined){$scope.newTransaction = '';}
		if($scope.newCounter != '' && $scope.newTransaction != '' ){
		  	generalFactory.postHandler('/frontline/changeCounter', { counter: $scope.newCounter, transaction: $scope.newTransaction }, function(response){
		  		if(response.success == 'ACTIVE'){
		  			alert(response.message);
	  				$scope.$parent.changedCounter = false;
		  		}else{
				  		if(response.success == 'ERROR'){
							$scope.message = 'Error on data save';
						} else {
							console.log($rootScope);
							$scope.$parent.counterChanged();
						}
				}
		  	});
	  	}else{
	  		$uibModalInstance.dismiss('cancel');
	  	}
	};


	$scope.jump = function(){
		$scope.$parent.jumped();
		if(!$scope.counter){
			alert('Please set your counter number first.');
		}else{
			generalFactory.postHandler('/frontline/jump', {number : $scope.skippednumber}, function(response){
	  			if(response.success == 'ACTIVE'){
	  				alert(response.message);
	  			}else{
	  				$scope.$parent.refreshFront();
	  			}

		  		/*$scope.$parent.seconds = 0;
		  		$scope.$parent.minutes = 0;
		  		$scope.$parent.hours = 0;

    			$scope.$parent.startTime();*/

				$uibModalInstance.dismiss('cancel');
			}).then(function(){
				generalFactory.getHandler('/frontline/setSafe', function(response){
					console.log(response.message);
				});

			});;
		}
	};

	$scope.cancel = function () {
	   	$uibModalInstance.dismiss('cancel');
	};


	$scope.$on('closeModal', function(e) {
       	$uibModalInstance.dismiss('cancel');
    });
}]);

app.directive('frontline', function(){
    return {
        restrict: 'E',
        templateUrl: 'partials/frontline.html',
        controller: 'FrontlineController'
    };
});

app.directive('backroom', function(){
    return {
        restrict: 'E',
        templateUrl: 'partials/backroom.html',
        controller: 'BackroomController'
    };
});

app.directive('reporitorial', function(){
    return {
        restrict: 'E',
        templateUrl: 'partials/reportmenu.html',
        controller: 'ReportController'
    };
});

app.directive('userlist', function(){
    return {
        restrict: 'E',
        templateUrl: 'partials/user_list.html',
        controller: 'AdminController'
    };
});

app.directive('userpermissions', function(){
    return {
        restrict: 'E',
        templateUrl: 'partials/user_permissions.html',
        controller: 'AdminController'
    };
});

app.directive('usergroups', function(){
    return {
        restrict: 'E',
        templateUrl: 'partials/user_groups.html',
        controller: 'AdminController'
    };
});


app.directive('navdirective', ['$timeout', function ($timeout) {
  return {
    link: function ($scope, element, attrs) {
    	$('.sidemenu').click(function(){
			$('.sidemenu').removeClass('active');
			$(this).addClass('active');
		});
    }
  };
}]);
app.directive('subnavdirective', ['$timeout', function ($timeout) {
  return {
    link: function ($scope, element, attrs) {
		$('.sub').click(function(){
			$('.sub').removeClass('sub_active');
			$(this).addClass('sub_active');
		});
    }
  };
}]);
